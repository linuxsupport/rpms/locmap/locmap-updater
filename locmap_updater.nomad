job "${PREFIX}_locmap_updater" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_locmap_updater" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/locmap_updater/locmap_updater:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_locmap_updater"
        }
      }
      volumes = [
        "/etc/nomad/linuxci_pw:/etc/linuxci.credential",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ADMIN_EMAIL = "$ADMIN_EMAIL"
      MODULES = "$MODULES"
      MR_BRANCH = "$MR_BRANCH"
    }

    resources {
      cpu = 3000 # Mhz
      memory = 2048 # MB
    }

  }
}
