SCHEDULE="30 5 * * *"
ADMIN_EMAIL="morrice@cern.ch"
MR_BRANCH="dev_locmap_updater"
MODULES="afs augeas augeasproviders_core augeasproviders_pam chrony concat cvmfs eosclient firewalld inifile postfix selinux stdlib systemd"
# uncomment to test on just a subset of modules
#MODULES="postfix"

