# Locmap Updater

This is a simple script to ease the update of locmap modules from their 'upstream' counterparts

The script clones both the upstream and locmap version and runs an `rsync` between the two to determine any differences.

For modules that have custom code to support the locmap use case, patches are applied should they exist in the `patches` directory.

If a module was deemed to have an update, the spec file version is bumped and a MR is created for the module in question.
