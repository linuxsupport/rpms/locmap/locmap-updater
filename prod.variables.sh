SCHEDULE="30 5 * * *"
ADMIN_EMAIL="lxsoft-admins@cern.ch"
MR_BRANCH="locmap_updater"
MODULES="afs augeas augeasproviders_core augeasproviders_pam chrony concat cvmfs eosclient firewalld inifile postfix selinux stdlib systemd"
