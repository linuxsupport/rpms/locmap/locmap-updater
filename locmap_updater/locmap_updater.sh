#!/bin/bash

# Allow MODULE to be passed as a command line arguement
# (useful for testing purposes)
if [ $# -eq 1 ]; then
  MODULES=$1
fi

function do_admin_email {
  SUBJECT=$1
  BODY=$2
  MAILMX=cernmx.cern.ch
  FROM="Linux.Support@cern.ch"

  MAILTMP=$(/bin/mktemp /tmp/$DIST$$.XXXXXXXX)
  echo "To: $ADMIN_EMAIL" >> $MAILTMP
  echo "From: linux.support@cern.ch" >> $MAILTMP
  echo "Reply-To: noreply.Linux.Support@cern.ch" >> $MAILTMP
  echo "Return-Path: $ADMIN_EMAIL" >> $MAILTMP
  echo "Subject: $SUBJECT" >> $MAILTMP

  echo -e "\nDear admins,\n" >> $MAILTMP
  # ensure shell globbing doesn't mess up our email
  set -f
  echo -e $BODY >> $MAILTMP
  set +f
  echo -e "\n---\nBest regards,\nCERN Linux Droid\n(on behalf of the friendly humans of Linux Support)" >> $MAILTMP

  cat $MAILTMP | swaks --server $MAILMX --to $ADMIN_EMAIL --from $FROM --helo cern.ch --data -
  rm -f $MAILTMP

}

if [ -f /etc/linuxci.credential ]; then
  cat /etc/linuxci.credential | kinit linuxci &>/dev/null
  if [ $? -ne 0 ]; then
    echo "Failed to kinit, exiting"
    exit 1
  fi
fi

if [ ! -z $NOMAD_ALLOC_ID ]; then
  git config --global http.emptyAuth true
  git config --global user.name "CERN Linux Droid"
  git config --global user.email "linux.ci@cern.ch"
fi

if [ -z $ADMIN_EMAIL ]; then
  ADMIN_EMAIL="lxsoft-admins@cern.ch"
fi

for MODULE in $MODULES
do
    echo "working on $MODULE"
    UPSTREAM_CLONE_DIR=$(mktemp -d)
    LOCMAP_CLONE_DIR=$(mktemp -d)
    git clone https://:@gitlab.cern.ch:8443/ai/it-puppet-module-${MODULE}.git $UPSTREAM_CLONE_DIR &>/dev/null || FAILED_CLONE=1
    git clone https://:@gitlab.cern.ch:8443/linuxsupport/rpms/locmap/puppet-${MODULE}.git $LOCMAP_CLONE_DIR &>/dev/null || FAILED_CLONE=1
    if [ $FAILED_CLONE ]; then
      echo "$MODULE failed to clone for some reason, bailing"
      continue
    fi
    git -C $LOCMAP_CLONE_DIR branch -a | grep $MR_BRANCH &>/dev/null
    if [ $? -eq 0 ]; then
        echo "locmap-updater branch/MR already exists for puppet-${MODULE}, skipping"
        continue
    fi
    git -C $UPSTREAM_CLONE_DIR checkout master
    rsync --delete -a --exclude='.*' --exclude 'spec/*' $UPSTREAM_CLONE_DIR/code/ $LOCMAP_CLONE_DIR/src/code/
    if [ -f /root/patches/$MODULE ]; then
	echo "Applying patch for $MODULE"
        patch -d $LOCMAP_CLONE_DIR -p0 --ignore-whitespace --no-backup-if-mismatch < /root/patches/$MODULE
        if [ $? -ne 0 ]; then
            echo "Patch failed, exiting"
	    do_admin_email "locmap-updater: Patch for puppet-${MODULE} failed" "The patch for puppet-${MODULE} failed. Unfortunately for us both, a real brain will be needed to investigate.\n\nApologies for the inconvenience."
            exit 1
        fi
    fi
    # avoid "file does not contain a valid yaml hash" warnings in puppet
    YAML_FILES=$(find $LOCMAP_CLONE_DIR/src/code -name *yaml)
    for YAML_FILE in $YAML_FILES
    do
        hiera -y $YAML_FILE 2>&1 | grep -q "Please supply a data item to look up"
	if [ $? -ne 0 ]; then
	    echo "file $YAML_FILE is not valid and will be deleted"
	    rm -f $YAML_FILE
	fi
    done
    FILES_CHANGED=$(git -C $LOCMAP_CLONE_DIR status --porcelain | wc -l)
    if [ $FILES_CHANGED -eq 0 ]; then
        echo "puppet-${MODULE} is already current, skipping"
        continue
    fi
    pushd $LOCMAP_CLONE_DIR &>/dev/null
    CURRENT_HEAD=$(git -C $UPSTREAM_CLONE_DIR rev-parse origin/master | cut -c -8)
    git checkout -b $MR_BRANCH
    git -C $LOCMAP_CLONE_DIR add *
    OLD_VERSION=$(grep -E '^Version:' puppet-*.spec | awk '{print $2}')
    MAJOR_VERSION=$(echo "$OLD_VERSION" | cut -d. -f1)
    MINOR_VERSION=$(echo "$OLD_VERSION" | cut -d. -f2)
    NEW_MINOR_VERSION=$((MINOR_VERSION + 1))
    NEW_VERSION="${MAJOR_VERSION}.${NEW_MINOR_VERSION}"
    CHANGELOG_DATE=$(date +"%a %b %d %Y")
    CHANGELOG_ENTRY="* ${CHANGELOG_DATE} CERN Linux Droid <linux.ci@cern.ch> - $NEW_VERSION-1\n- Rebased to #${CURRENT_HEAD} by locmap-updater\n"
    sed -i -e "s/^Version:.*/Version:        ${NEW_VERSION}/" -e "s/^Release: .*/Release:        1%{?dist}/"  -e "/^%changelog/a ${CHANGELOG_ENTRY}" puppet-*.spec
    git commit -am "Code rebased to #${CURRENT_HEAD} by locmap-updater" &>/dev/null # redirect can be removed
    echo "Updated puppet-${MODULE} at ${LOCMAP_CLONE_DIR}"
    # TODO: maybe if we are confident also set "-o merge_request.merge_when_pipeline_succeeds"
    MR_URL=$(git -C $LOCMAP_CLONE_DIR push origin $MR_BRANCH  --progress -o merge_request.create -o merge_request.remove_source_branch 2>&1 |grep merge_requests | awk '{print $2}')
    popd &>/dev/null
    do_admin_email "locmap-updater: New MR created for puppet-${MODULE}" "I have detected upstream code changes and have created a new MR for puppet-${MODULE} which can be consulted at ${MR_URL}.\n\nYou may want to click on a button or something, just saying."
done
